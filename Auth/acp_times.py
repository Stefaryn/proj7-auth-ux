"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    # Tables
    distances = [0, 200, 400, 600, 1000]
    max_speeds = [34, 32, 30, 28, 26]

    # Round kilometers to nearest kilometer
    control_dist_km = int(control_dist_km + .5) 
    brevet_dist_km = int(brevet_dist_km + .5) 

    # Check if over brevet limit
    if (control_dist_km > brevet_dist_km):
        tmp_dist = brevet_dist_km
    else:
        tmp_dist = control_dist_km

    # Main algorithm
    hour_change = 0.0
    for i in reversed(range(len(distances))):
        if (tmp_dist > distances[i]):
            amount_greater = tmp_dist - distances[i]
            hour_change += float(amount_greater) / float(max_speeds[i])
            tmp_dist -= amount_greater

    # Get minute and hour change
    minute_change = (hour_change - int(hour_change)) * 60
    hour_change = int(hour_change)
    # Round minute change to nearest minute
    minute_change = int(minute_change + .5) 

    # Get start time and shift 
    start_arrow = arrow.get(brevet_start_time)
    start_arrow = start_arrow.shift(hours=+hour_change) 
    start_arrow = start_arrow.shift(minutes=+minute_change)

    return start_arrow.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    # Tables
    distances = [0, 200, 400, 600, 1000]
    min_speeds = [15, 15, 15, 11.428, 13.333]

    # Round kilometers to nearest kilometer
    control_dist_km = int(control_dist_km + .5) 
    brevet_dist_km = int(brevet_dist_km + .5) 

    # Check if over brevet limit
    if (control_dist_km > brevet_dist_km):
        tmp_dist = brevet_dist_km
    else:
        tmp_dist = control_dist_km

    # Check for special case of 200 km
    if (tmp_dist == 200 and brevet_dist_km == 200):
        case_200 = True
    else:
        case_200 = False    

    # Main algorithm
    hour_change = 0.0
    for i in reversed(range(len(distances))):
        if (tmp_dist > distances[i]):
            amount_greater = tmp_dist - distances[i]
            hour_change += float(amount_greater) / float(min_speeds[i])
            tmp_dist -= amount_greater

    # Get minute and hour change
    minute_change = (hour_change - int(hour_change)) * 60
    hour_change = int(hour_change) 
    # Round minute change to nearest minute
    minute_change = int(minute_change + .5) 

    # Check special close case
    if (hour_change < 1):
        hour_change = 1
        minute_change = 0

    # Check special 200 km case
    if (case_200):
        hour_change = 13
        minute_change = 30

    # Get start time and shift 
    start_arrow = arrow.get(brevet_start_time)
    start_arrow = start_arrow.shift(hours=+hour_change) 
    start_arrow = start_arrow.shift(minutes=+minute_change) 

    return start_arrow.isoformat()
