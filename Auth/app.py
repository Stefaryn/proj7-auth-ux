import os
from flask import Flask, redirect, url_for, request, render_template
from flask_restful import Resource, Api
from pymongo import MongoClient
import datetime
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from passlib.apps import custom_app_context as pwd_context
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin, 
                            confirm_login, fresh_login_required)
from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import time

import acp_times

app = Flask(__name__)

SECRET_KEY = "doot doot"
DEBUG = True
app.config.from_object(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
opendb = client.opendb
closedb = client.closedb
passwordsdb = client.passwordsdb

def hash_password(password):
    return pwd_context.encrypt(password)

def verify_password(password, hashVal):
    return pwd_context.verify(password, hashVal)

api = Api(app)

# User class
class User(UserMixin):
    def __init__(self, name, id, active=True):
        self.name = name
        self.id = id
        self.active = active

    def is_active(self):
        return self.active

    def is_authenticated(self):
        return (self.id != 0)

login_manager = LoginManager()
login_manager.setup_app(app)

class RegistrationForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')

class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    remember = BooleanField('Remember Me')

cur_user=User("", 0, False)

@login_manager.user_loader
def load_user(id):
    return cur_user

@login_manager.unauthorized_handler
def unathorized():
    # Ask for a log in
    return redirect(url_for('login'))

cur_token = None

def generate_auth_token(expiration=600):
    global cur_user
    tmp_id = cur_user.id
    s = Serializer(str(cur_user.id), expires_in=expiration)

    global cur_token
    cur_token = s.dumps({'id': int(tmp_id)})

    return s.dumps({'id': int(tmp_id)})

def verify_auth_token(token):
    if (token == None):
        return False
    global cur_user
    tmp_id = cur_user.id
    if (tmp_id == 0):
        return False

    s = Serializer(str(cur_user.id))
    try:
        data = s.loads(token)
    except SignatureExpired:
        return False    # valid token, but expired
    except BadSignature:
        return False    # invalid token
    return True

static_formid = 1

@app.route("/api/register", methods=["GET", "POST"])
def register():
    form = RegistrationForm()
    if request.method == 'POST':
        username = form.username.data
        password = form.password.data
        hashed = hash_password(password)

        global static_formid 
        db_user = { "id": str(static_formid), "username": username, "password": hashed }
        static_formid = static_formid + 1
        passwordsdb.passwordsdb.insert_one(db_user)

        return render_template("registered.html")

    return render_template("register.html", form=form)

@app.route("/api/token", methods=["GET", "POST"])
@login_required
def token():
    generate_auth_token()
    return render_template("token.html")
 
@app.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()

    if request.method == 'POST':
        username = form.username.data
        password = form.password.data
        remember = form.password.data

        from_db = passwordsdb.passwordsdb.find_one( { 'username': username } )
        try:
            stored = from_db['password']
        except:
            return render_template('unregistered.html') 

        if (verify_password(password, stored)):
            global cur_user
            cur_user = User(username, from_db['id'])
            if (remember):
                login_user(cur_user, remember = True)
            else:
                login_user(cur_user)
            return render_template('in.html')
        else:
            return render_template('failed.html')

    return render_template("login.html", form=form)

@app.route("/logout", methods=["GET", "POST"])
def logout():
    global cur_user
    cur_user = User("", 0, False)
    cur_token = None
    logout_user()
    return render_template("logout.html")

@app.route('/')
def todo():
    return render_template('new.html')

@app.route('/display', methods=['POST'])
def display():
    # Check token
    if (verify_auth_token(cur_token)):
        pass
    else:
        return render_template('bad_token.html')     
    # Get all items from database and send to display
    open_times = opendb.opendb.find()
    close_times = closedb.closedb.find()
  
    open_items = [item for item in open_times]
    close_items = [item for item in close_times]
 
    items = open_items + close_items

    # If empty give a message
    if (len(items) == 0):
        return render_template('empty.html')

    return render_template('display.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    # Get all the args we use
    try:
        km = request.form["km"]
        km = float(km)
        location_name = request.form["location_name"]
        brevet_distance = request.form["distance"]
        brevet_distance = float(brevet_distance)
        begin_date = request.form["begin_date"]
        begin_time = request.form["begin_time"]
    except:
        # If any of those aren't found show this instead
        return render_template('bad_input.html')

    # Get start datetime formatted
    start_datetime = begin_date + "T" + begin_time

    # Call time calc functions
    open_time = acp_times.open_time(km, brevet_distance, start_datetime)
    close_time = acp_times.close_time(km, brevet_distance, start_datetime)
    open_time = "Open Time: " + open_time
    close_time = "Close Time: " + close_time


    # Format results
    open_result = {"location": location_name, "time": open_time}
    close_result = {"location": location_name, "time": close_time}

    # Insert into db and continue
    opendb.opendb.insert_one(open_result)
    closedb.closedb.insert_one(close_result)
    return render_template('new.html')

# Resources for api
class listAll(Resource):
    def get(self):
        # Check token
        if (verify_auth_token(cur_token)):
            pass
        else:
            return render_template('bad_token.html')
        # Now show data     
        open_times = opendb.opendb.find()
        close_times = closedb.closedb.find()
  
        open_items = [item for item in open_times]
        close_items = [item for item in close_times]
 
        items = open_items + close_items
        # Sort ascending
        items.sort(key=lambda k: k['time'])

        tmp = []
        for item in items:
            tmp.append({item['location']: item['time']})

        return tmp

class listOpenOnly(Resource):
    def get(self):
        # Check token
        if (verify_auth_token(cur_token)):
            pass
        else:
            return render_template('bad_token.html')
        # Now show data     
        open_times = opendb.opendb.find()
  
        open_items = [item for item in open_times]
 
        items = open_items
        # Sort ascending
        items.sort(key=lambda k: k['time'])

        tmp = []
        top = int(request.args.get('top', default=len(items)))
        if top > len(items):
             top = len(items)
        for i in range(top):
            tmp.append({items[i]['location']: items[i]['time']})
 
        return tmp

class listCloseOnly(Resource):
    def get(self):
        # Check token
        if (verify_auth_token(cur_token)):
            pass
        else:
            return render_template('bad_token.html')
        # Now show data     
        close_times = closedb.closedb.find()
  
        close_items = [item for item in close_times]
 
        items = close_items
        # Sort ascending
        items.sort(key=lambda k: k['time'])

        tmp = []
        top = int(request.args.get('top', default=len(items)))
        if top > len(items):
             top = len(items)
        for i in range(top):
            tmp.append({items[i]['location']: items[i]['time']})
 
        return tmp

class listAlljson(Resource):
    def get(self):
        # Check token
        if (verify_auth_token(cur_token)):
            pass
        else:
            return render_template('bad_token.html')
        # Now show data     
        open_times = opendb.opendb.find()
        close_times = closedb.closedb.find()
  
        open_items = [item for item in open_times]
        close_items = [item for item in close_times]
 
        items = open_items + close_items
        # Sort ascending
        items.sort(key=lambda k: k['time'])

        tmp = []
        for item in items:
            tmp.append({item['location']: item['time']})
 
        return tmp

class listOpenOnlyjson(Resource):
    def get(self):
        # Check token
        if (verify_auth_token(cur_token)):
            pass
        else:
            return render_template('bad_token.html')
        # Now show data     
        open_times = opendb.opendb.find()
  
        open_items = [item for item in open_times]
 
        items = open_items
        # Sort ascending
        items.sort(key=lambda k: k['time'])

        tmp = []
        top = int(request.args.get('top', default=len(items)))
        if top > len(items):
             top = len(items)
        for i in range(top):
            tmp.append({items[i]['location']: items[i]['time']})
 
        return tmp

class listCloseOnlyjson(Resource):
    def get(self):
        # Check token
        if (verify_auth_token(cur_token)):
            pass
        else:
            return render_template('bad_token.html')
        # Now show data     
        close_times = closedb.closedb.find()
  
        close_items = [item for item in close_times]
 
        items = close_items
        # Sort ascending
        items.sort(key=lambda k: k['time'])

        tmp = []
        top = int(request.args.get('top', default=len(items)))
        if top > len(items):
             top = len(items)
        for i in range(top):
            tmp.append({items[i]['location']: items[i]['time']})
 
        return tmp

class listAllcsv(Resource):
    def get(self):
        # Check token
        if (verify_auth_token(cur_token)):
            pass
        else:
            return render_template('bad_token.html')
        # Now show data     
        open_times = opendb.opendb.find()
        close_times = closedb.closedb.find()
  
        open_items = [item for item in open_times]
        close_items = [item for item in close_times]
 
        items = open_items + close_items
        # Sort ascending
        items.sort(key=lambda k: k['time'])

        # Set up in csv format
        tmp = ["Location, Time"]
        for item in items:
            tmp.append(item['location'] + ", " + item['time'])
 
        return tmp

class listOpenOnlycsv(Resource):
    def get(self):
        # Check token
        if (verify_auth_token(cur_token)):
            pass
        else:
            return render_template('bad_token.html')
        # Now show data     
        open_times = opendb.opendb.find()
  
        open_items = [item for item in open_times]
 
        items = open_items
        # Sort ascending
        items.sort(key=lambda k: k['time'])

        # Set up in csv format
        tmp = ["Location, Time"]
        top = int(request.args.get('top', default=len(items)))
        if top > len(items):
             top = len(items)
        for i in range(top):
            tmp.append(items[i]['location'] + ", " + items[i]['time'])
 
        return tmp

class listCloseOnlycsv(Resource):
    def get(self):
        # Check token
        if (verify_auth_token(cur_token)):
            pass
        else:
            return render_template('bad_token.html')
        # Now show data     
        close_times = closedb.closedb.find()
  
        close_items = [item for item in close_times]
 
        items = close_items
        # Sort ascending
        items.sort(key=lambda k: k['time'])

        # Set up in csv format
        tmp = ["Location, Time"]
        top = int(request.args.get('top', default=len(items)))
        if top > len(items):
             top = len(items)
        for i in range(top):
            tmp.append(items[i]['location'] + ", " + items[i]['time'])
 
        return tmp

# Default links
api.add_resource(listAll, '/listAll')
api.add_resource(listOpenOnly, '/listOpenOnly')
api.add_resource(listCloseOnly, '/listCloseOnly')

# Json links
api.add_resource(listAlljson, '/listAll/json')
api.add_resource(listOpenOnlyjson, '/listOpenOnly/json')
api.add_resource(listCloseOnlyjson, '/listCloseOnly/json')

# CSV links
api.add_resource(listAllcsv, '/listAll/csv')
api.add_resource(listOpenOnlycsv, '/listOpenOnly/csv')
api.add_resource(listCloseOnlycsv, '/listCloseOnly/csv')

if __name__ == "__main__":
    static_formid = 1
    app.run(host='0.0.0.0', debug=True)
