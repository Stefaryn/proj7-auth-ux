# Project 7 for cis 322: Authentication based Brevet time calculator with Ajax and MongoDB and REST

Author: Stefan Fields, sfields@uoregon.edu

This implements an ACP brevet time calculator, to run you just use the
run.sh script to build a docker image and run the webpage at
http://localhost:5000

Authentication:

    To view the data in the database, either with the display button or
    the API's mentioned below, you must now go through an authentication
    process. This happens in 3 main steps.

    Step 1 is registering a user, this is done at /api/register (after
    the normal link) and you enter a username and password to be hashed and
    stored in a users database.

    Step 2 is logging in with that registration, either by going to /login
    or just going straight to step 3 where you will be rerouted to login

    Step 3 is getting your authentication token by going to /api/token
    this requires login from a registered user (steps 1 and 2), but after
    you are able to view all the data as normal. 

REST API:

    In addition to the previous methods of display, you can now
    enter specific links to access the data once it has been submitted

    Specifically you can use the links http://localhost:5000/<example>
    where example can be listAll listOpenOnly or listCloseOnly to see
    all times, open times only, or close times only. You can also specificy
    the format by after those entering /json or /csv (though json is default)
    finally you can use an argument ?top=<n> (specifiying an int) and
    OpenOnly and CloseOnly with show you the first n times in ascending order 

Buttons:

    To use you enter information for each control time, and then
    submit it (where it is stored in a database) and when you are done
    press display and everything in that database will be presented to you. 

    If press display when nothing is in the database you will see
    a message describing that the database is empty.

    If you press submit with missing or invalid information a 
    message will describe that you have entered invalid input.

Implementation Specifics:
    The general algorithmn works off of the tables of min and
    max speeds over a given distance (given in the table).

Table:

    Location(km) Mininum Speed (km/hr) Maximim Speed (km/hr)
    0-200       | 15                  | 34
    200-400     | 15                  | 32
    400-600     | 15                  | 30
    600-1000    | 11.428              | 28
    1000-1300   | 13.333              | 26

    Basically any distance over 1000 on a control is calculated with
    the speeds on the 1000 part of the table, and then you continue to
    drop down and do anything from 600-1000 on the table with those speeds
    and so on until all the time is calculated

Edge Case Description:

    The algorithm also always rounds the number of kilometers and
    the number of minutes to the nearest whole before calculating time.

    If a control distance would be longer than the brevet distance of the
    race as a whole, the control distance's time will instead be calculated
    from the brevet distance. This is because the races have overall time
    limits for each brevet distance. However organizers should not organize
    races with end much longer than the brevet distance.

    If a closing time would be less than an hour after the opening time
    of the whole race, it is instead one hour after the start of the race.
    This is for the case of control points very shortly into the race and
    control points at 0. These cases should genrally be avoided. There is
    also a different approach to this orgianlly used if France.

    Finally there is a special case where if a race has a brevet distance
    of 200km, the final closing time for it will be 13H30 after the start
    instead of the 13H20 the algorithm would calculate.
    
